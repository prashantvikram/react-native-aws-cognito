import React from "react";
import { Component } from 'react';

import { StyleSheet, Text, View, TextInput, Button } from 'react-native';

import Amplify, { Auth } from 'aws-amplify';
import { withAuthenticator } from "aws-amplify-react"
import aws_exports from '../aws-exports';
Amplify.configure(aws_exports);

import { NavigationScreenProp } from "react-navigation";

export interface Props {
  navigation: NavigationScreenProp<any, any>
}
export interface State {
  phone_number: string,
  confirmation_code: string,
}
class ConfirmSignup extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    const { navigation } = this.props;
    this.state = {
      phone_number: navigation.getParam('phone_number', ""),
      confirmation_code: ""
    }
  }
  onChangeText(key: any, value: string) {
    this.setState({ confirmation_code: value });
  }
  confirmSignUp() {
    Auth.confirmSignUp(this.state.phone_number, this.state.confirmation_code)
      .then(() => {
        console.log('confirmed sign up')
        this.props.navigation.navigate('Launch');
      })
      .catch(err => console.log('error ', err))
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>Enter the confirmation code!</Text>
        <TextInput
          onChangeText={value => this.onChangeText('confirmation_code', value)}
          keyboardType="number-pad"
        />
        <Button
          onPress={this.confirmSignUp.bind(this)}
          title="Sign up"
          accessibilityLabel="Sign up"
        />
      </View>
    );
  }
}

export default ConfirmSignup;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  }
});
