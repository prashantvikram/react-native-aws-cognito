import React from "react";
import { Component } from 'react';

import { StyleSheet, Text, View, TextInput, Button } from 'react-native';

import Amplify, { Auth } from 'aws-amplify';
import aws_exports from '../aws-exports';
Amplify.configure(aws_exports);

import { NavigationScreenProp } from "react-navigation";

export interface Props {
  navigation: NavigationScreenProp<any, any>
}
export interface State {
  phone_number: string,
  password: string,
}
class SignupPhonePass extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      phone_number: "",
      password: "",
    }
  }
  onChangeText(key: any, value: string) {
    switch (key) {
      case "phone_number":
        this.setState({ phone_number: value });
        break;
      case "password":
        this.setState({ password: value });
        break;
      default:
        break;
    }
  }
  signUp() {
    Auth.signUp({
      username: this.state.phone_number,
      password: this.state.password,
      attributes: {
        phone_number: this.state.phone_number
      }
    })
      .then(() => {
        console.log(this.state.phone_number);
        this.props.navigation.navigate('ConfirmSignup', {
          phone_number: this.state.phone_number
        });
      })
      .catch(err => console.log('error: ', err))
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>Welcome to React Native!</Text>
        <TextInput
          onChangeText={value => this.onChangeText('phone_number', value)}
          autoFocus={true}
          keyboardType="phone-pad"
        />
        <TextInput
          onChangeText={value => this.onChangeText('password', value)}
          secureTextEntry={true}
        />
        <Button
          onPress={this.signUp.bind(this)}
          title="Get Confirmation Code"
          accessibilityLabel="Get Confirmation Code"
        />
      </View>
    );
  }
}

export default SignupPhonePass;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  }
});
