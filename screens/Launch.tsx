import React from "react";
import { Component } from 'react';
import { Text } from "react-native"

import { NavigationScreenProp } from "react-navigation";

export interface Props {
  navigation: NavigationScreenProp<any, any>
}

export interface State {
  isAuthenticated: boolean
}

class Launch extends Component<Props, State>{
  componentWillMount(){
    // check if the user is logged in
    // may be from Async Storage
    // and set the state of isAuthenticated
    this.setState({ isAuthenticated: false })
  }
  render(){
    if (this.state.isAuthenticated) {
      // show main screen
      return (
        <Text>Launch</Text>
      )
    } 
    return (
      this.props.navigation.navigate('SignupPhonePass')
    )
  }
}

export default Launch