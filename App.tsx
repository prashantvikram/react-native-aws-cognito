import React from "react";
import { Component } from "react";
import { createStackNavigator } from "react-navigation";

// import screens
import Launch from "./screens/Launch";
import SignupPhonePass from "./screens/SignupPhonePass";
import ConfirmSignup from "./screens/ConfirmSignup";

const config = {
  Launch: { screen: Launch },
  SignupPhonePass: { screen: SignupPhonePass },
  ConfirmSignup: { screen: ConfirmSignup }
}

const options = {
  initialRouteName: 'Launch'
}

const Navigator = createStackNavigator(config, options);
export interface Props {}
export interface State {}

class App extends Component<Props, State>{
  constructor(props: Props){
    super(props);
  }
  render(){
    return(
      <Navigator />
    )
  }
}
export default App;